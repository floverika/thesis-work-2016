<?php

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Регистрация участников/////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

$IamHere='Регистрация';

 echo $content = 
 '
 	<div class= "reg_body">
 		
 		<span> <p>  Добро пожаловать в Союз IT-Безопасность! </p> </span>
 		
 		<p> Вступайте в Наш союз и присоединятесь к группе преданных специалистов, решающих  проблемы в области информационной безопасности сегодняшнего и завтрашнего дня!  </p> 

 		<p> Вам будут доступны множество бесплатных сервисов нашего портала. А каких, спросите Вы? Для подробной информации перейдите, пожалуйста, по следующей ссылке - <a style="color:#20b0c8;" href="Index.php?page=aim" >О Нас</a>.</p>

 		<p>При регистрации, просим Вас указывать корректные и полные данные для более эффективного взаимодействия между участниками портала. </p>
 		<p>Обязательные поля отмечены символом "*".</p> 

 		
 	</div>';


//Выводим сообщение об удачной регистрации
 if(isset($_GET['status']) and $_GET['status'] == 'ok')
{
	echo '<b>Вы успешно зарегистрировались! Теперь вы можете войти на сайт </b>';
}
 

	
 /*Если нажата кнопка на регистрацию,
 начинаем проверку*/
 if(isset($_POST['submit']))
 {
	
	if(empty($_POST['email']))
		$err[] = 'Поле Email не может быть пустым!';
	else
	{
		if(!preg_match("/^[a-z0-9_.-]+@([a-z0-9]+\.)+[a-z]{2,6}$/i", $_POST['email']))
           $err[] = 'Не правильно введен E-mail'."\n";
	}
	
	if(empty($_POST['pass']))
		$err[] = 'Поле Пароль не может быть пустым';
	
	if(empty($_POST['pass2']))
		$err[] = 'Поле Подтверждения пароля не может быть пустым';
	
	//Проверяем наличие ошибок и выводим пользователю
	if(count($err) > 0)
		echo showErrorMessage($err);
	else
	{
		/*Продолжаем проверять введеные данные
		Проверяем на совподение пароли*/
		if($_POST['pass'] != $_POST['pass2'])
			$err[] = 'Пароли не совподают';
			
		//Проверяем наличие ошибок и выводим пользователю
	    if(count($err) > 0)
			echo showErrorMessage($err);
		else
		{
			/*Проверяем существует ли у нас 
			такой пользователь в БД*/
			mysqli_query ($link, "SELECT `login` 
					FROM `reg`
					WHERE `login` =  '". mysqli_real_escape_string($link,$_POST['email']) ."'"
							);
			
			
			if (mysqli_affected_rows($link) > 0)
				$err[] = 'К сожалению Логин: <b>'. $_POST['email'] .'</b> занят!';
			
			//Проверяем наличие ошибок и выводим пользователю
			if(count($err) > 0)
				echo showErrorMessage($err);
			else
			{
				//Получаем ХЕШ соли
				$salt = salt();
				
				//Солим пароль
				$pass = md5(md5($_POST['pass']).$salt);
				
				/*Если все хорошо, пишем данные в базу*/
				mysqli_query ($link,'INSERT INTO `reg`
						VALUES(
								"",
								"'. mysqli_real_escape_string($link,$_POST['email']) .'",
								"'. $pass .'",
								"'. $salt .'",
								"'. md5($salt) .'"
								)'
								);			
				//Сбрасываем параметры
				header('Location:'. HOST .'?page=reg&status=ok');
				exit;
			}
		}
	}
 }
 
?>



<form class="reg_form" action="" method="POST">
	
	<p style="color: #222222;  font-weight: bold;">ЛИЧНЫЕ ДАННЫЕ</p>
 	<p class="fieldset">Имя:<span style="color:red; margin-left: 0px">*</span><input type="text" class="input_login" name="first_name"></p>
	<p class="fieldset">Фамилия:<span style="color:red;margin-left: 0px">*</span><input type="text" class="input_login" name="last_name"></p>
	<p class="fieldset">Отчество: <input type="text" class="input_login" name="midle_name"></p>
	<p class="fieldset">Email:<span style="color:red;margin-left: 0px">*</span><input type="email"  class="input_login" name="email"></p>
	<p class="fieldset">Пароль:<span style="color:red;margin-left: 0px">*</span><input type="password"  class="input_login" name="pass"></p>
	<p class="fieldset">Повторите пароль:<span style="color:red;margin-left: 0px">*</span><input type="password" class="input_login" name="pass2"></p>
	<p class="fieldset">Фотография: <input  size=70 type="file" name="img_profile"> </p>
	<p style="border-bottom: 2px solid #CCC ; margin-bottom: 10px"></p>
	<p style="color: #222222;  font-weight: bold;">КОМПАНИЯ </p>
 	<p class="fieldset">Компания:<span style="color:red;margin-left: 0px">*</span><input type="text" class="input_login" name="name_company"></p>
	<p><input style="margin-left: 25%" type="checkbox"  name="vehicle" value="Bike">Я являюсь независимым специалистом</p>
	<p class="fieldset">Направление:<input type="text" class="input_login" name="activity"></p>
	<p class="fieldset">Регион: <input type="text" class="input_login" name="region"></p>
	<p class="fieldset">Город:<input type="text"  class="input_login" name="city"></p>
	<p class="fieldset">Фактический адрес:<input type="text"  class="input_login" name="actual_adress"></p>
	<p class="fieldset">Сайт компании:<input type="text" class="input_login" name="url_company"></p>
	<p class="fieldset">Email компании:<input type="email" class="input_login" name="email_company"></p>
	<p style="border-bottom: 2px solid #CCC; margin-bottom: 10px"></p>
	<p style="color: #222222;  font-weight: bold;">ОБРАЗОВАНИЕ </p>
	<p class="fieldset">Учебное заведение:<input type="text"  class="input_login" name="name_ed"></p>
	<p class="fieldset">Специальность:<input type="text"  class="input_login" name="specialty"></p>
	<p class="fieldset">Ученая степень:
		<select>
        	<option selected> -</option>
       		<option>Кандидат наук</option>
        	<option>Доктор наук</option>
    	</select> </p>
	</p><input type="submit" class="submit" value="Зарегистрироваться" name="submit"></p>
</form>



