<html>
<head> 
	<title>Information security</title> 
	
	<link rel="stylesheet" href="./css/main.css" type="text/css"> 
	<link rel="stylesheet" href="./css/bd.css" type="text/css">
	<link rel="stylesheet" href="./css/article.css" type="text/css">
	<link rel="stylesheet" href="./css/article_form.css" type="text/css">
	<link rel="stylesheet" href="./css/trydiSpiran.css" type="text/css">
	<link rel="stylesheet" href="./css/page_aim.css" type="text/css">

</head>
<body> 

<div id="page_align">

<!--**************************HEADER********************************-->	
<!-- create header -->
	
	<div id="header">
		
		<div id="header_top">
		
			<div class="reg_and_auth">
				<div class="menu">
					<ul>
						<li><a   class="signin" href="Index.php?page=auth">Вход</a></li>
						<li><a  href="Index.php?page=reg" >Регистрация</a></li>
					</ul>
				</div>
			</div>
			<h1>
				<a style="color: #fff" href="Index.php">Союз IT-Безопасность<a>
			</h1>
			
			<span class="h1description">Обмен инновационными решениями</span>
		</div>

<!--Create nav-->	
		<ul id="header_nav">
			<li> 
				<a href="#">О нас</a> 
				<ul>
					<li><a href="Index.php?page=aim">Цели</a></li>
					<li><a href="#">Участники</a></li>
					<li><a href="#">Правила</a></li>
					<li><a href="#"><i></i>Полезные ссылки</a></li>
				</ul>
			</li>
			<li> 
				<a href="#">Журнал</a>
					<ul>
						<li><a href="#">Книги</a></li>
						<li><a href="Index.php?page=article">Статьи</a></li>
						<li><a href="#">Документация</a></li>
					</ul>
			 </li>
			<li> <a href="#">Предложения</a></li>
			<li> <a href="#">Решения</a> </li>
			<li> <a href="./forum">Форум</a> </li>
			<li><a href="#"><i></i>Объявления</a></li>
			<li> <a href="#">Контакты</a> </li>

		</ul>
	</div>
<!--**************************END HEADER********************************-->	
<div class="box"><span> <a href="Index.php">Главная</a> / <?php  echo $pg = $IamHere;?> 

</span></div>
<!--****************************SIDEBAR**********************************-->	
	
	<!--Create sidebar-->
	<div id="sidebar">	
		<div class="sb_header">НОВОСТИ</div>
		<div class="sb_nav">  
			<!--Запрос на вывод информаии о добавленной статье-->	

			<? // SQL-запрос
			 //Подключаем MySQL
			include './php/main_articles.php';
			?>
		</div>
		<div class="sb_header">СОБЫТИЯ</div> 
		<div class="sb_nav"> </div>			
	</div>
<!--******************************END SIDEBAR******************************-->	

<!--******************************CONTENT**********************************-->	
	<div id="content">
		<div class="content_body">
			<?php echo $content; ?>
		</div>
	
	</div>
<!--******************************END CONTENT******************************-->		
	<div class="clr"></div>

	<!--******************************FOOTER******************************-->	
	<div id="footer">
		<div class="footer_body"> 
		
		</div>
	</div>
<!--******************************END FOOTER******************************-->	

</div>
</body>
	<div class="clr"></div>

</html>

