<?php

	//Запускаем сессию
	session_start();

	//Устанавливаем кодировку и вывод всех ошибок
	header('Content-Type: text/html; charset=utf-8');
	error_reporting(E_ALL);

	//Включаем буферизацию содержимого
	ob_start();

	//Определяем переменную для переключателя
	$page = isset($_GET['page'])  ? $_GET['page'] : false;
	$user = isset($_SESSION['user']) ? $_SESSION['user'] : false;
	$err = array();
	
	//Cоздаем хлебные крошки
	$pg='';
	$IamHere='';

	//Инициализация нашей БД
	include './config.php';

	//Подключаем MySQL
	include './php/bd.php';
	
	switch($page)
	{
		
		//Шаблон с выводом статей
		case  'article':
			include './php/articles.php';
		break;
		//Шаблон добавление статей
		case  'add_article':
			include './php/add.php';
		break;
		//Регистрация участников
		case 'reg':
			include './php/reg.php';
		break;

		//Авторизация участников
		case 'auth':
			include './php/auth.php';
			include './php/show.php';
		break;
		//Информация о союзе (цель союза)
		case 'aim':
			include './php/aim.php';
		break;
	}   
	
	//Получаем данные с буфера
	$content = ob_get_contents();
	ob_end_clean();
	include './php/main.php';
?>



	